package com.sys0x.fit.bi_psi.BI_PSI_DU1;

public class Robot {
    private final Integer ID;
    private String name;

    public Robot(Integer ID) {
        this.ID = ID;
    }

    public Integer getID() {
        return ID;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}

package com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums;

public enum Heading {
    NORTH( 1,0),
    SOULTH(-1,0),
    EAST(0,1),
    WEST(0,-1);


    public final Integer x;
    public final Integer y;


    Heading(Integer x, Integer y) {
        this.x=x;
        this.y=y;
    }
}

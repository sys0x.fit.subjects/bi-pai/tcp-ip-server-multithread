package com.sys0x.fit.bi_psi.BI_PSI_DU1;

import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.Constants;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.MessageType;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions.NotServerMessage;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions.ServerLogicException;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions.SyntaxErrorException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Processor {
    private static Logger logger= LogManager.getLogger(Handler.class);
    private InputStream input;
    private BufferedWriter writer;
    Socket comunicationSocket;

    Processor(Socket comunicationSocket) throws IOException{
        this.comunicationSocket = comunicationSocket;
        input=comunicationSocket.getInputStream();
        writer = new BufferedWriter(new OutputStreamWriter(comunicationSocket.getOutputStream()));
    }
    public void endComunication() throws IOException {
        comunicationSocket.close();
    }

    private String getText(Integer messageLength) throws IOException, SyntaxErrorException {
        StringBuilder message= new StringBuilder();
        while(true){
            message.append((char)input.read());
            if(message.toString().contains("\u0007\u0008")){
                break;
            }
            if(message.length()>=messageLength){
                logger.error("Message was too long");
                throw new SyntaxErrorException("Message was too long; Stopped reading after "+messageLength+" number of characters, and received:"+message.toString(),MessageType.UNKNOWN);
            }
        }
        logger.debug("Received message: "+message.toString());
        return message.toString();
    }

    private Message getMessage(MessageType messageType, Integer messageLength) throws IOException, SyntaxErrorException, ServerLogicException {

        String messageText=getText(messageLength);

        Message newMessage;
        try {
            newMessage = new Message(messageText,false,messageType);
        } catch ( SyntaxErrorException syntax) {
            if(syntax.getMessageDetected().equals(MessageType.CLIENT_RECHARGING)){
                newMessage = handelRobotRecharge(messageType,messageLength);
            }else{
                throw new SyntaxErrorException("Message should be: "+messageType+" but was: "+syntax.getMessageDetected(),syntax.getMessageDetected());
            }
        }

        return newMessage;
    }
    public Message getMessage(MessageType messageType) throws IOException, SyntaxErrorException, ServerLogicException {
        if(messageType.maxLength<12)
        return getMessage(messageType,12);
        else{
            return getMessage(messageType,messageType.maxLength);
        }
    }


    private Message handelRobotRecharge(MessageType messageType, Integer messageLength) throws IOException, ServerLogicException, SyntaxErrorException {
        comunicationSocket.setSoTimeout(Constants.TIMEOUT_RECHARGING.getValue());
        try {
            getMessage(MessageType.CLIENT_FULL_POWER);
        } catch (SyntaxErrorException e) {
            if(!e.getMessageDetected().equals(MessageType.UNKNOWN)){
                throw new ServerLogicException("After client recharge should be full pover but was: "+e.getMessageDetected());
            }
        }
        comunicationSocket.setSoTimeout(Constants.TIMEOUT.getValue());
        return getMessage(messageType,messageLength);
    }

    public void sendMessage(Message message) throws IOException {
        try {
            writer.write(message.sendMessage());
            writer.flush();
        } catch (NotServerMessage notServerMessage) {
            logger.error("WUT learn to code, your trying to send a non server message");
            logger.error(notServerMessage.getStackTrace());
        }
    }
}

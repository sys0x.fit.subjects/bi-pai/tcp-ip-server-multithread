package com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions;

public class OutOfTimeException extends Exception {
    public OutOfTimeException(String errorMessage) {
        super(errorMessage);
    }

}

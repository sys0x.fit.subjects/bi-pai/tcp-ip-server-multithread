package com.sys0x.fit.bi_psi.BI_PSI_DU1;

import com.sys0x.fit.bi_psi.BI_PSI_DU1.Actions.ClientActions;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Actions.ServerActions;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.Heading;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.MessageType;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions.LoginFailedException;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions.ServerLogicException;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions.SyntaxErrorException;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.Socket;

import static java.lang.Math.abs;


public class Handler implements Runnable {
    private static Logger logger = LogManager.getLogger(Handler.class);
    private Robot robot;
    private Processor processor;

    Handler(Socket robotSocket, int ID) throws IOException {
        robot = new Robot(ID);
        processor = new Processor(robotSocket);
    }
    public int getRobotID() {
        return robot.getID();
    }

    private void authenticate() throws IOException, LoginFailedException, SyntaxErrorException, ServerLogicException {
        Message message = processor.getMessage(MessageType.CLIENT_USERNAME);
        ClientActions.CLIENT_USERNAME(message, robot);

        Message serverConfirmationMessage = ServerActions.SERVER_CONFIRMATION(message.toSting());
        processor.sendMessage(serverConfirmationMessage);

        Message clientConfirmationMessage = processor.getMessage(MessageType.CLIENT_CONFIRMATION);

        if (ClientActions.CLIENT_CONFIRMATION(clientConfirmationMessage, serverConfirmationMessage)) {
            processor.sendMessage(ServerActions.SERVER_OK());
        } else {
            throw new LoginFailedException("Login failed should recived ");
        }
    }


    private Message moveHandler(Message lastOK) throws IOException, ServerLogicException, SyntaxErrorException {
        while (true) {
            processor.sendMessage(ServerActions.SERVER_MOVE());
            Message okClient = processor.getMessage(MessageType.CLIENT_OK);
            if(!okClient.toSting().equals(lastOK.toSting())){
                return okClient;
            }
        }
    }

    private Heading rightHeading(Heading heading){
        switch (heading){
            case WEST:
                return Heading.NORTH;

            case EAST:
                return Heading.SOULTH;

            case SOULTH:
                return Heading.WEST;

            case NORTH:
                return Heading.EAST;

            default:
                return Heading.NORTH;
        }
    }

    private Pair<Message, Heading> decider(Message okClientNew, Heading heading,Pair<Integer,Integer> whereToGo){
        Pair<Integer,Integer> currentXY=ClientActions.CLIENT_OK(okClientNew);

        if(currentXY.equals(whereToGo)){
            return new Pair<>(ServerActions.SERVER_PICK_UP(),heading);
        }
        Pair<Integer,Integer> futureXY = new Pair<>(currentXY.getKey()+heading.x,currentXY.getValue()+heading.y);

        switch (heading){
            case NORTH:
            case SOULTH:
                if(abs(whereToGo.getKey()-futureXY.getKey())<abs(whereToGo.getKey()-currentXY.getKey())){
                    return new Pair<>(ServerActions.SERVER_MOVE(),heading);
                }
                break;
            case EAST:
            case WEST:
                if(abs(whereToGo.getValue()-futureXY.getValue())<abs(whereToGo.getValue()-currentXY.getValue())){
                    return new Pair<>(ServerActions.SERVER_MOVE(),heading);
                }
                break;
        }


        heading=rightHeading(heading);
        return new Pair<>(ServerActions.SERVER_TURN_LEFT(),heading);


    }

    private Heading headingDeciderer(Message okClientOld,Message okClientNew){
        Pair<Integer,Integer> XYOLD=ClientActions.CLIENT_OK(okClientOld);
        Pair<Integer,Integer> XYNEW=ClientActions.CLIENT_OK(okClientNew);
        if(!XYOLD.getKey().equals(XYNEW.getKey())){
            if(XYOLD.getKey()<XYNEW.getKey()){
                return Heading.NORTH;
            }else{
                return Heading.SOULTH;
            }
        }else {
            if(XYOLD.getValue()<XYNEW.getValue()){
                return Heading.EAST;
            }else{
                return Heading.WEST;
            }
        }

    }
    public Pair<Pair<Message, Heading>,Message> moveToAndPickup(Message currentPossition,Pair<Integer,Integer> whereToGo,Heading heading) throws SyntaxErrorException, ServerLogicException, IOException {
        logger.debug("currentPossition: "+currentPossition.toSting()+" heading: " + heading.toString()+" next order: UNKNOWN");
        while(true){
            Pair<Message, Heading> nextOrder = decider(currentPossition,heading,whereToGo);
            heading=nextOrder.getValue();
            if(nextOrder.getKey().messageType.equals(MessageType.SERVER_PICK_UP)){
                return new Pair<>(nextOrder,currentPossition);
            }else if(nextOrder.getKey().messageType.equals(MessageType.SERVER_MOVE)){
                currentPossition = moveHandler(currentPossition);
            }else{
                processor.sendMessage(nextOrder.getKey());
                currentPossition=processor.getMessage(MessageType.CLIENT_OK);
            }
            logger.debug("currentPosition: "+currentPossition.toSting()+" heading: " + heading.toString()+" order was:"+nextOrder.getKey().toSting());
        }

    }
    public Message moveToAndPickupStart() throws SyntaxErrorException, ServerLogicException, IOException {

        processor.sendMessage(ServerActions.SERVER_MOVE());
        Message firstPossition = processor.getMessage(MessageType.CLIENT_OK);
        Message currentPosition = moveHandler(firstPossition);
        Heading heading = headingDeciderer(firstPossition,currentPosition);
        int reverse=-1;
        for(int x=2;x>=-2;x--) {
            for (int y = 2 * reverse; y >= -2 && y <= 2; y -= reverse) {
                logger.debug("going to x:" + x + " y:" + y);
                Pair<Pair<Message, Heading>, Message> pickupMessage = moveToAndPickup(currentPosition, new Pair<>(x, y), heading);
                heading = pickupMessage.getKey().getValue();
                currentPosition = pickupMessage.getValue();
                processor.sendMessage(pickupMessage.getKey().getKey());
                Message clientMessage = processor.getMessage(MessageType.CLIENT_MESSAGE);
                if (!clientMessage.toSting().isEmpty()) {
                    processor.sendMessage(ServerActions.SERVER_LOGOUT());
                    return clientMessage;
                }
            }
            reverse *= -1;
        }


        return ServerActions.SERVER_LOGIC_ERROR();
    }

    public void run() {
        try {

            try {

                authenticate();
                Message seacret=moveToAndPickupStart();
                logger.info(seacret.toSting());
            } catch (SyntaxErrorException syntex) {
                logger.error(syntex.getMessage());
                processor.sendMessage(ServerActions.SERVER_SYNTAX_ERROR());
            } catch (LoginFailedException e) {
                logger.error(e.getMessage());
                processor.sendMessage(ServerActions.SERVER_LOGIN_FAILED());
            } catch (ServerLogicException e) {
                logger.error(e.getMessage());
                processor.sendMessage(ServerActions.SERVER_LOGIC_ERROR());
            } finally {
                processor.endComunication();

            }
        } catch (IOException e) {
            logger.error("Robot ID:" + robot.getID() + " something went wrong");
            logger.debug(e.getMessage());
        }


    }

}

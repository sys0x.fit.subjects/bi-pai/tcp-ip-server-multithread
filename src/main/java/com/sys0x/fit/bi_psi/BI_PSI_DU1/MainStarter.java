package com.sys0x.fit.bi_psi.BI_PSI_DU1;

import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

@SpringBootApplication
public class MainStarter {

	private static final Logger logger= LogManager.getLogger(MainStarter.class);
	public static void main(String[] args) {

		SpringApplication.run(MainStarter.class, args);

		logger.info("Server Starting...");

		ServerSocket serverSocket;
		try {
			logger.debug("ServerSocket -> starting on port:"+ Constants.PORT_NUMBER.getValue());
			serverSocket = new ServerSocket(Constants.PORT_NUMBER.getValue());
			logger.info("Server Started...");
			logger.debug("ServerSocket -> started on port:"+Constants.PORT_NUMBER.getValue());

			int robotID = 0;
			while (true) {
				logger.debug("ServerSocket -> starting listening for robot ");
				Socket clientSocket = serverSocket.accept();
				clientSocket.setSoTimeout(Constants.TIMEOUT.getValue());
				logger.debug("ServerSocket -> found robot ");
				logger.debug("Handler -> creating robotCommunicator with ID: "+robotID);
				Handler newCommunication = new Handler(clientSocket,robotID);
				robotID++;
				logger.debug("Handler ID:'"+ newCommunication.getRobotID() +"' -> creating new thread");
				new Thread(newCommunication).start();

			}
		} catch (IOException e) {
			logger.error("Failed -> IOException");
			logger.debug(e.getMessage());
		}

	}

}

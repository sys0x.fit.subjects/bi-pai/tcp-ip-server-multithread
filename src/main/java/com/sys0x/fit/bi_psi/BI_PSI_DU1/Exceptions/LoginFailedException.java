package com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions;

public class LoginFailedException extends Exception {
    public LoginFailedException(String errorMessage){
        super(errorMessage);
    }

}

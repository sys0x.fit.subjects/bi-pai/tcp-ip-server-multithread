package com.sys0x.fit.bi_psi.BI_PSI_DU1.Actions;

import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.Constants;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.MessageType;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions.SyntaxErrorException;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Handler;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ServerActions {
    private static Logger logger = LogManager.getLogger(Handler.class);

    public static Message SERVER_CONFIRMATION(String stringToConfirm) {
        long messageHash=(((stringToConfirm.chars().sum()*1000)%Constants.MODULO.getValue())+ Constants.SERVER_KEY.getValue())%Constants.MODULO.getValue();
        Message newMessage=null;
        try {
            newMessage= new Message(String.valueOf(messageHash),true, MessageType.SERVER_CONFIRMATION);
        } catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
    public static Message SERVER_MOVE(){
        Message newMessage=null;
        try {
            newMessage= new Message(MessageType.SERVER_MOVE.example,true,MessageType.SERVER_MOVE);
        }catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
    public static Message SERVER_TURN_LEFT(){
        Message newMessage=null;
        try {
            newMessage= new Message(MessageType.SERVER_TURN_LEFT.example,true,MessageType.SERVER_TURN_LEFT);
        } catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
    public static Message SERVER_TURN_RIGHT(){
        Message newMessage=null;
        try {
            newMessage= new Message(MessageType.SERVER_TURN_RIGHT.example,true,MessageType.SERVER_TURN_RIGHT);
        } catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
    public static Message SERVER_PICK_UP(){
        Message newMessage=null;
        try {
            newMessage= new Message(MessageType.SERVER_PICK_UP.example,true,MessageType.SERVER_PICK_UP);
        } catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
    public static Message SERVER_LOGOUT(){
        Message newMessage=null;
        try {
            newMessage= new Message(MessageType.SERVER_LOGOUT.example,true,MessageType.SERVER_LOGOUT);
        } catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
    public static Message SERVER_OK(){
        Message newMessage=null;
        try {
            newMessage= new Message(MessageType.SERVER_OK.example,true,MessageType.SERVER_OK);
        } catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
    public static Message SERVER_LOGIN_FAILED() {
        Message newMessage=null;
        try {
            newMessage= new Message(MessageType.SERVER_LOGIN_FAILED.example,true,MessageType.SERVER_LOGIN_FAILED);
        } catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
    public static Message SERVER_SYNTAX_ERROR(){
        Message newMessage=null;
        try {
            newMessage= new Message(MessageType.SERVER_SYNTAX_ERROR.example,true,MessageType.SERVER_SYNTAX_ERROR);
        } catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
    public static Message SERVER_LOGIC_ERROR() {
        Message newMessage=null;
        try {
            newMessage= new Message(MessageType.SERVER_LOGIC_ERROR.example,true,MessageType.SERVER_LOGIC_ERROR);
        } catch (SyntaxErrorException e) {
            logger.fatal("THE IMPOSABLE HAPPENED");
            logger.fatal(e.getMessage());
            e.getMessage();
        }
        return newMessage;
    }
}

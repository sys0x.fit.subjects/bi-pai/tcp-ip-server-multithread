package com.sys0x.fit.bi_psi.BI_PSI_DU1.Actions;

import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.Constants;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Handler;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Message;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Robot;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ClientActions {
    private static Logger logger = LogManager.getLogger(Handler.class);
    public static void CLIENT_USERNAME(Message message, Robot robot)  {
        robot.setName(message.toSting());

    }

    public static boolean CLIENT_CONFIRMATION(Message newMessage, Message originalMessage){
        long originalHash=(Long.parseLong(originalMessage.toSting())+Constants.MODULO.getValue()-Constants.SERVER_KEY.getValue())%Constants.MODULO.getValue();
        long clientHash=(originalHash+ Constants.CLIENT_KEY.getValue())%Constants.MODULO.getValue();
        String clientMessageShouldBe=String.valueOf(clientHash);
        if(!newMessage.toSting().equals(clientMessageShouldBe)){
           logger.debug("Should recive"+clientHash+"but was"+newMessage.toSting());
        }
        return newMessage.toSting().equals(clientMessageShouldBe);

    }
    public static Pair<Integer,Integer> CLIENT_OK(Message newMessage){
        String numbers= newMessage.toSting();
        numbers=numbers.replace("[^-?0-9]+", " ");
        String [] intArray=numbers.split(" ");
        return new Pair<>(Integer.valueOf(intArray[1]),Integer.valueOf(intArray[2]));

    }

    public static boolean CLIENT_MESSAGE(){
        return true;
    }

}

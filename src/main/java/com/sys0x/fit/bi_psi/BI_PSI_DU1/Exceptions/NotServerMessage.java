package com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions;

public class NotServerMessage extends Exception {
    public NotServerMessage(String errorMessage){
        super(errorMessage);
    }

}

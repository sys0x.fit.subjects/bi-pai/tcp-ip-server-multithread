package com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions;

public class ServerLogicException extends Exception {
    public ServerLogicException(String errorMessage){
        super(errorMessage);
    }
}

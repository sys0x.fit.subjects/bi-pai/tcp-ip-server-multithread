package com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums;

public enum Constants {
    TIMEOUT(1000),
    TIMEOUT_RECHARGING(5000),
    SERVER_KEY(54621),
    CLIENT_KEY(45328),
    MODULO(65536),
    PORT_NUMBER(6666);

    private Integer value;

    Constants(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}

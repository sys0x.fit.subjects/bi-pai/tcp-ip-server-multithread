package com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums;

public enum MessageType {
    SERVER_CONFIRMATION("^[0-9]{0,5}\u0007\u0008$","",7,true,false),
    SERVER_MOVE("^102 MOVE\u0007\u0008$","102 MOVE\u0007\u0008",10,true,false),
    SERVER_TURN_LEFT("^103 TURN LEFT\u0007\u0008$","103 TURN LEFT\u0007\u0008",15,true,false),
    SERVER_TURN_RIGHT("^104 TURN RIGHT\u0007\u0008$","104 TURN RIGHT\u0007\u0008",16,true,false),
    SERVER_PICK_UP("^105 GET MESSAGE\u0007\u0008$","105 GET MESSAGE\u0007\u0008",17,true,false),
    SERVER_LOGOUT("^106 LOGOUT\u0007\u0008$","106 LOGOUT\u0007\u0008",12,true,false),
    SERVER_OK("^200 OK\u0007\u0008$","200 OK\u0007\u0008",8,true,false),
    SERVER_LOGIN_FAILED("^300 LOGIN FAILED\u0007\u0008$","300 LOGIN FAILED\u0007\u0008",18,true,false),
    SERVER_SYNTAX_ERROR("^301 SYNTAX ERROR\u0007\u0008$","301 SYNTAX ERROR\u0007\u0008",18,true,false),
    SERVER_LOGIC_ERROR("^302 LOGIC ERROR\u0007\u0008$","302 LOGIC ERROR\u0007\u0008",17,true,false),
    CLIENT_USERNAME("^.{0,10}\u0007\u0008$","",12,false,false),
    CLIENT_CONFIRMATION("^[0-9]{0,5}\u0007\u0008$","",7,false,true),
    CLIENT_OK("^OK [-]{0,1}[0-9]{1,2} [-]{0,1}[0-9]{1,2}\u0007\u0008$","",12,false,true),
    CLIENT_RECHARGING("^RECHARGING\u0007\u0008$","",12,false,true),
    CLIENT_FULL_POWER("^FULL POWER\u0007\u0008$","",12,false,true),
    CLIENT_MESSAGE("^.{0,98}\u0007\u0008$","",100,false,false),
    UNKNOWN("^UNKNOWN MESSAGE$","",100,false,false);


    public final String regex;
    public final String example;
    public final Integer maxLength;
    public final boolean serverMessage;
    public final boolean canAnalyze;

    MessageType(String regex, String example, Integer maxLength, boolean serverMessage, boolean canAnalyze) {
        this.regex = regex;
        this.maxLength = maxLength;
        this.serverMessage = serverMessage;
        this.canAnalyze = canAnalyze;
        this.example = example;
    }



}

package com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions;

import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.MessageType;

public class SyntaxErrorException extends Exception {
    MessageType detected;
    public SyntaxErrorException(String errorMessage,MessageType type){
        super(errorMessage);
        this.detected=type;
    }

    public MessageType getMessageDetected() {
        return detected;
    }
}

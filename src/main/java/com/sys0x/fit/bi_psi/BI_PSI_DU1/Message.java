package com.sys0x.fit.bi_psi.BI_PSI_DU1;

import com.sys0x.fit.bi_psi.BI_PSI_DU1.Enums.MessageType;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions.NotServerMessage;
import com.sys0x.fit.bi_psi.BI_PSI_DU1.Exceptions.SyntaxErrorException;

public class Message {
    String message;
    MessageType messageType;
    boolean serverMessage;

    private void analyzeMessage() throws SyntaxErrorException {
        for(MessageType type : MessageType.values()){
            if(type.canAnalyze&&message.matches(type.regex)){
                messageType=type;
            }
        }
        if(messageType.equals(MessageType.UNKNOWN)){
            throw new SyntaxErrorException("Could not figure out what type of message this:\""+message+"\" was",MessageType.UNKNOWN);
        }
    }

    private void verifyMessage() throws SyntaxErrorException {
        if(message.matches(MessageType.CLIENT_RECHARGING.regex)){
            throw new SyntaxErrorException("Message type should match"+messageType.regex+" but doesn't message was"+MessageType.CLIENT_RECHARGING,MessageType.CLIENT_RECHARGING);
        }
        if(!message.matches(messageType.regex)){
            messageType=MessageType.UNKNOWN;
            analyzeMessage();
            throw new SyntaxErrorException("Message type should match"+messageType.regex+" but doesn't message was"+messageType,messageType);
        }
    }
    public Message(String message, boolean serverMessage,MessageType messageType) throws SyntaxErrorException {
        if(!message.contains("\u0007\u0008")){
            message+="\u0007\u0008";
        }
        this.message=message;
        this.serverMessage =serverMessage;
        this.messageType = messageType;
        if(messageType.equals(MessageType.UNKNOWN)){
            analyzeMessage();
        }else{
            verifyMessage();
        }
    }

    public String sendMessage() throws NotServerMessage {
        if(!messageType.serverMessage){
            throw new NotServerMessage("Trying to send "+messageType);
        }
        if(!message.contains("\u0007\u0008")){
            return message+"\u0007\u0008";
        }

        return message;
    }
    public String toSting(){
        if(message.contains("\u0007\u0008")){
            return message.replace("\u0007\u0008","");
        }
        return message;
    }

}
